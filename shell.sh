#!/bin/bash

#Get a shell to the container in the pod for testing / debugging

kubectl exec -it $(kubectl get po | grep demo | awk '{ print $1 }') -- /bin/bash

