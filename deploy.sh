#!/bin/bash
  


# Uncomment to build the Go app once your environment is configured for dependencies
# GOOS=linux go build -o ./app .  

eval $(minikube docker-env)
docker build -t in-cluster .
kubectl create clusterrolebinding default-view --clusterrole=view --serviceaccount=default:default
kubectl run --rm -i demo --image=in-cluster --image-pull-policy=Never
