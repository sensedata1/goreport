#!/bin/bash -x

# This will delete then recompile the Go app, rebuild the container, then deploy to minikube.
# Please verify your environment and dependencies are configured before running this script and deleting the app
# in the process. 

rm app
kubectl delete -n default deployment demo
GOOS=linux go build -o ./app .
eval $(minikube docker-env)
docker build -t in-cluster .
kubectl create clusterrolebinding default-view --clusterrole=view --serviceaccount=default:default
kubectl run --rm -i demo --image=in-cluster --image-pull-policy=Never

