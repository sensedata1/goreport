goreport

In-cluster pod count for "default namespace and pod data dump. 

Requisites:

kubectl
minikube


Run ./deploy to build container and deploy to minikube to start counting those pods

If you wish to rebuild / modify the Go app please see the client-go README.md
https://github.com/kubernetes/client-go/blob/master/INSTALL.md
